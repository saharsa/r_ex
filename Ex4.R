setwd("C:/Users/Daniella/Desktop/������/�������/��� �/����� �/����� ��� ������ �����/�������/Ex4")
aus.raw<-read.csv("weatherAUS.csv")
summary(aus.raw)

aus.prepared <-aus.raw

make_average <- function(x,xvec){
  if(is.na(x)){
    return (mean(xvec, na.rm=TRUE))
  }
  return (x)
}

aus.prepared$Evaporation<-NULL
aus.prepared$Sunshine<-NULL
aus.prepared$RISK_MM<-NULL

aus.prepared$MinTemp <-sapply(aus.prepared$MinTemp, make_average, xvec = aus.prepared$MinTemp)
aus.prepared$MaxTemp <-sapply(aus.prepared$MaxTemp, make_average, xvec = aus.prepared$MaxTemp)
aus.prepared$Rainfall <-sapply(aus.prepared$Rainfall, make_average, xvec = aus.prepared$Rainfall)
aus.prepared$WindGustSpeed <-sapply(aus.prepared$WindGustSpeed, make_average, xvec = aus.prepared$WindGustSpeed)
aus.prepared$WindSpeed3pm <-sapply(aus.prepared$WindSpeed3pm, make_average, xvec = aus.prepared$WindSpeed3pm)
aus.prepared$WindSpeed9am <-sapply(aus.prepared$WindSpeed9am, make_average, xvec = aus.prepared$WindSpeed9am)
aus.prepared$Humidity9am <-sapply(aus.prepared$Humidity9am, make_average, xvec = aus.prepared$Humidity9am)
aus.prepared$Humidity3pm <-sapply(aus.prepared$Humidity3pm, make_average, xvec = aus.prepared$Humidity3pm)
aus.prepared$Pressure9am <-sapply(aus.prepared$Pressure9am, make_average, xvec = aus.prepared$Pressure9am)
aus.prepared$Pressure3pm <-sapply(aus.prepared$Pressure3pm, make_average, xvec = aus.prepared$Pressure3pm)
aus.prepared$Cloud3pm <-sapply(aus.prepared$Cloud3pm, make_average, xvec = aus.prepared$Cloud3pm)
aus.prepared$Cloud9am <-sapply(aus.prepared$Cloud9am, make_average, xvec = aus.prepared$Cloud9am)
aus.prepared$Temp9am <-sapply(aus.prepared$Temp9am, make_average, xvec = aus.prepared$Temp9am)
aus.prepared$Temp3pm <-sapply(aus.prepared$Temp3pm, make_average, xvec = aus.prepared$Temp3pm)


summary(aus.prepared)

make_unknown <- function(x){
  if(is.na(x)) return ("unknown")
  return(x)
}

aus.prepared$RainToday <- as.character(aus.prepared$RainToday)
aus.prepared$RainToday <- sapply(aus.prepared$RainToday,make_unknown)
aus.prepared$RainToday <- as.factor(aus.prepared$RainToday)
summary(aus.prepared$RainToday)


aus.prepared$WindGustDir <- as.character(aus.prepared$WindGustDir)
aus.prepared$WindGustDir <- sapply(aus.prepared$WindGustDir,make_unknown)
aus.prepared$WindGustDir <- as.factor(aus.prepared$WindGustDir)
summary(aus.prepared$WindGustDir)

aus.prepared$WindDir9am <- as.character(aus.prepared$WindDir9am)
aus.prepared$WindDir9am <- sapply(aus.prepared$WindDir9am ,make_unknown)
aus.prepared$WindDir9am <- as.factor(aus.prepared$WindDir9am)
summary(aus.prepared$WindDir9am)

aus.prepared$WindDir3pm <- as.character(aus.prepared$WindDir3pm)
aus.prepared$WindDir3pm <- sapply(aus.prepared$WindDir3pm ,make_unknown)
aus.prepared$WindDir3pm <- as.factor(aus.prepared$WindDir3pm)
summary(aus.prepared$WindDir3pm)

summary(aus.prepared)

str(aus.prepared)

aus.prepared$Date <- as.character(aus.prepared$Date)

month <- as.factor(substr(aus.prepared$Date,4,5))
aus.prepared$month <- month 

year <- as.factor(substr(aus.prepared$Date,7,10))
aus.prepared$year <- year

aus.prepared$Date <- NULL

library(ggplot2)

ggplot(aus.prepared) +aes(x = MinTemp, fill = RainTomorrow) + geom_bar(position = "fill")#*
aus.prepared$MinTemp <- NULL

ggplot(aus.prepared) +aes(x = MaxTemp, fill = RainTomorrow) + geom_bar(position = "fill")#***

ggplot(aus.prepared) +aes(x = Rainfall, fill = RainTomorrow) + geom_bar(position = "fill")#***

ggplot(aus.prepared) +aes(x = WindGustDir, fill = RainTomorrow) + geom_bar(position = "fill")#*
aus.prepared$WindGustDir <- NULL

ggplot(aus.prepared) +aes(x = WindGustSpeed, fill = RainTomorrow) + geom_bar(position = "fill")#***

ggplot(aus.prepared) +aes(x = WindDir9am, fill = RainTomorrow) + geom_bar(position = "fill")#*
aus.prepared$WindDir9am <- NULL

ggplot(aus.prepared) +aes(x = WindDir3pm, fill = RainTomorrow) + geom_bar(position = "fill")#*
aus.prepared$WindSpeed3pm <-NULL

ggplot(aus.prepared) +aes(x = Pressure9am, fill = RainTomorrow) + geom_bar(position = "fill")#***

ggplot(aus.prepared) +aes(x = Pressure3pm, fill = RainTomorrow) + geom_bar(position = "fill")#
aus.prepared$Pressure3pm <- NULL

ggplot(aus.prepared) +aes(x = Cloud9am, fill = RainTomorrow) + geom_bar(position = "fill")#**

ggplot(aus.prepared) +aes(x = Cloud3pm, fill = RainTomorrow) + geom_bar(position = "fill")#**

ggplot(aus.prepared) +aes(x = Temp3pm, fill = RainTomorrow) + geom_bar(position = "fill")#**

ggplot(aus.prepared) +aes(x = Temp9am, fill = RainTomorrow) + geom_bar(position = "fill")#*
aus.prepared$Temp9am <-NULL

ggplot(aus.prepared) +aes(x = RainToday, fill = RainTomorrow) + geom_bar(position = "fill")#*
aus.prepared$RainToday <- NULL

ggplot(aus.prepared) +aes(x = month, fill = RainTomorrow) + geom_bar(position = "fill")#
aus.prepared$month <- NULL 

ggplot(aus.prepared) +aes(x = year, fill = RainTomorrow) + geom_bar(position = "fill")#
aus.prepared$year <- NULL

ggplot(aus.prepared) +aes(x = Location, fill = RainTomorrow) + geom_bar(position = "fill")

str(aus.prepared)

library(caTools)

filter <- sample.split(aus.prepared$Location , SplitRatio = 0.7)

aus.train <- subset(aus.prepared, filter ==T)
aus.test <- subset(aus.prepared, filter ==F)

dim(aus.prepared)
dim(aus.train)
dim(aus.test)

aus.model <- glm(RainTomorrow ~ ., family = binomial(link = 'logit'), data = aus.train)

summary(aus.model)

step.model.income <- step(aus.model)
summary(step.model.income)

predicted.rain.test <- predict(aus.model, newdata = aus.test, type = 'response')

confusion_matrix <- table(predicted.rain.test >0.5, aus.test$RainTomorrow)

recall <- confusion_matrix[2,2]/(confusion_matrix[2,2] + confusion_matrix[1,2])
precision <- confusion_matrix[1,1]/(confusion_matrix[1,1] + confusion_matrix[2,1])
recall
precision
