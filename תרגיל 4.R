install.packages('ggplot2')
library(ggplot2)
install.packages("ggplot2movies")
library(ggplot2movies)
movie.fm <- ggplot2movies::movies
str(movie.fm)

# qus 1
ggplot(movie.fm,aes(movie.fm$rating)) + geom_histogram()
# ���� ����� �� ���� ������ ����� ������� �������
# ��� ������ ����� ���������� ������ ������ ����� �� ����� ������ ��� �6.5 
# ��� ������ ����� �� ������ ������� ��� ������� ������ ���� �� ������ ����� �� 
# ��� ������ ��� ��� ���� �� �-4.5 ��-7.5

# qus 2
ggplot(movie.fm, aes(movie.fm$year,movie.fm$rating)) + geom_point() + stat_smooth(method = lm)
# ��� ��� ������ ��"� ��� ������ ����� ���� ����� ������ ������ ����� ����� ������� ���� ���� ����

# qus 3 
ggplot(movie.fm, aes(movie.fm$year,movie.fm$budget)) + geom_point() + stat_smooth(method = lm)
# ��� ����� ��� �������� ���� ��� ��� ������ ���� ��� ���� �� ����� ����� ����� (�� �� ������ �� ������ ������)

# qus 4
ggplot(movie.fm, aes(movie.fm$length,movie.fm$budget)) + geom_col() + xlim(0,1000) + ylim(0,3000)


# qus 5
ggplot(movie.fm, aes(movie.fm$length,movie.fm$rating)) + geom_col() + xlim(0,1000) + ylim(0,3000)
# �� �� ���� ���� ����� �� ���� ���� �� ����� �� ������ 
# ����� ������ ���� ��� ��-200 ���� ������ ����� ���� ����
# ����� ����� ��� ������ ���� ���� ������ �� ������ ��� ����� 













