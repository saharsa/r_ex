titanic.df <- read.csv("titanic.csv")
str(titanic.df)
var <- as.vector(t(titanic.df))
var
# ���� 1 
nadf <- function(data.fm){
  a <- is.na.data.frame(data.fm)
  return(a)
}

# ���� 2
rep_NA <- function(vecr){
  med.row <- median(vecr,na.rm = T)
  vecr[is.na(vecr)] <- med.row
  return(vecr)
}

a <- c(5,6,NA)
a
median(a,na.rm = TRUE)
a[is.na(a)] <- 2 
rep_NA(a)
# ���� 3
max(titanic.df$SibSp)
max(titanic.df$Parch)
titanic.df$Survived <- factor(titanic.df$Survived, levels =0:1, labels = c("no","yes"))
titanic.df$Pclass <- factor(titanic.df$Pclass, levels = 1:3,labels = c("1st","2nd","3rd"))
titanic.df$SibSp <- factor(titanic.df$SibSp, levels = 0:8,labels = c("0sb","1sb","2sb","3sb","4sb","5sb","6sb","7sb","8sb"))
titanic.df$Parch <- factor(titanic.df$Parch, levels = 0:6,labels = c("0ch","1ch","2ch","3ch","4ch","5ch","6ch"))






